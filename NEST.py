#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 17:43:31 2020

@author: federico
"""
# %%
#Libraries
import nest
import nest.raster_plot                           # Nest graphs module
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler  # to PCA
from sklearn.decomposition import PCA             # to PCA
from mpl_toolkits import mplot3d                  # 3D plot

# %%
################################# General parameters

# simulation parameters
simtime = 19000.             # simulation time (ms)
dt = 1.                     # simulation resolution (ms)
#reps=1                     # number of repetitions of the simulation

# Neuron types
iaf="iaf_psc_alpha"
hh="hh_psc_alpha"
iafdelta="iaf_psc_delta"
ORN_type = iaf
LN_lat_type = iaf
LN_glo_type = iaf
PN_type = iaf

# Noise parameters

noise_std =200.
noise_step = {"dt":5.}

# Synapses parameters
exc_weight=500.
inh_weight=-150.

# stimuli parameters
st1_start=1000.         # stimuli 1
st1_stop=2000. 
st1_amp=300.
st1_std=50.
st1_glom=("ORN1","ORN2","ORN3","ORN4","ORN5","ORN6")
st1_weights=(1,1,1.5,1.5,1,0.3)

st2_start=3000.         # stimuli 2
st2_stop=4000.
st2_amp=300.
st2_std=50.
st2_glom=("ORN7","ORN8","ORN9","ORN10","ORN11","ORN12")
st2_weights=(0.3,1.0,1.5,1.5,1.0,0.5)

st3_start=5000.          # stimuli 3
st3_stop=6000.
st3_amp=300.
st3_std=50.
st3_glom=st1_glom+st2_glom
st3_weights=st1_weights+st2_weights

st4_start=17000.          # stimuli 4
st4_stop=18000.
st4_amp=300.
st4_std=50.
st4_glom=st3_glom
st4_weights=st3_weights

st5_start=7000.          # stimuli 5
st5_stop=16000.
st5_amp=300.
st5_std=50.
st5_glom=st1_glom
st5_weights=st1_weights

st6_start=000.          # stimuli 6
st6_stop=000.
st6_amp=350.
st6_std=50.
st6_glom=("ORN1","ORN2","ORN3","ORN4")
st6_weights=(1.0,1.0,1.0,0.5)

st7_start=000.          # stimuli 7
st7_stop=000.
st7_amp=350.
st7_std=50.
st7_glom=("ORN1","ORN2","ORN3","ORN4")
st7_weights=(1.0,1.0,1.0,0.5)

st8_start=000.          # stimuli 8
st8_stop=000.
st8_amp=350.
st8_std=50.
st8_glom=("ORN1","ORN2","ORN3","ORN4")
st8_weights=(1.0,1.0,1.0,0.5)

# random generator parameters
msd = np.random.randint(low=1,high=1e7)
N_vp = nest.GetKernelStatus(['total_num_virtual_procs'])[0]
pyrngs = [np.random.RandomState(s) for s in range(msd, msd+N_vp)]
    
# configure kernel
nest.ResetKernel()
nest.SetKernelStatus({
    'resolution': dt,      # set simulation resolution
    'print_time': True,    # enable printing of simulation progress (-> terminal)
    'local_num_threads': 1, # use n threads to build & simulate the network
    "grng_seed" : int(msd+N_vp),
    "rng_seeds" : range(msd+N_vp+1, msd+2*N_vp+1),
})
# %%
# NUMBER OF NEURONS

#anatomical structure
glomeruli=20

#for each glomeruli
nORN=8
nPN=3
nLN_lat=20
nLN_glo=2


# %%
#Neurons settings

               
neuron_params = {
#    'C_m': 1.44,           # membrane capacity (pF)
#    'E_L': -62.,           # resting membrane potential (mV)
#    'I_e': 0.,             # external input current (pA)
#    'V_m': -65.0,          # membrane potential (mV)
#    'V_reset': -62.0,      # reset membrane potential after a spike (mV)
#    'V_th': -55.0,         # spike threshold (mV)
    't_ref': 10.0,         # refractory period (ms)
#    'tau_m': 580.0,        #
#   "tau_minus": 20.0       # postsynaptic parameter for stpd
}
#
nest.SetDefaults('iaf_psc_alpha', neuron_params)
# %%
# Creating neurons
ORN = nest.Create(ORN_type, int(nORN*glomeruli))
LN_lat = nest.Create(LN_lat_type, int(nLN_lat*glomeruli))
PN = nest.Create(PN_type, int(nPN*glomeruli))
LN_glo = nest.Create(LN_glo_type, int(nLN_glo*glomeruli))

# Glomeruli neurons
glomeruli_list = {}
for i in range (glomeruli):
    glomeruli_list["ORN"+str(i+1)]=ORN[nORN*i:nORN*i+nORN]


#%%
################################# Creating noise generators to stimulate ORNs
## Noise generators ##
nest.SetDefaults("noise_generator",noise_step)
noise_gen = nest.Create("noise_generator", params={"mean":0., "std":noise_std}) # General noise
noise_gen_1 = nest.Create("noise_generator", params={"mean":st1_amp, "std":st1_std, "start":st1_start, "stop":st1_stop})
noise_gen_2 = nest.Create("noise_generator", params={"mean":st2_amp, "std":st2_std, "start":st2_start, "stop":st2_stop})
noise_gen_3 = nest.Create("noise_generator", params={"mean":st3_amp, "std":st3_std, "start":st3_start, "stop":st3_stop})
noise_gen_4 = nest.Create("noise_generator", params={"mean":st4_amp, "std":st4_std, "start":st4_start, "stop":st4_stop})
noise_gen_5 = nest.Create("noise_generator", params={"mean":st5_amp, "std":st5_std, "start":st5_start, "stop":st5_stop})
noise_gen_6 = nest.Create("noise_generator", params={"mean":st6_amp, "std":st6_std, "start":st6_start, "stop":st6_stop})
noise_gen_7 = nest.Create("noise_generator", params={"mean":st7_amp, "std":st7_std, "start":st7_start, "stop":st7_stop})
noise_gen_8 = nest.Create("noise_generator", params={"mean":st8_amp, "std":st8_std, "start":st8_start, "stop":st8_stop})


#print("Generators added")
################################# Creating detectors
# Spike detectors
sdetector_ORN = nest.Create("spike_detector")
nest.SetStatus(sdetector_ORN, {"withgid": True, "withtime": True, "to_file":False})

sdetector_LN_lat = nest.Create("spike_detector")
nest.SetStatus(sdetector_LN_lat, {"withgid": True, "withtime": True, "to_file":False})

sdetector_PN = nest.Create("spike_detector")
nest.SetStatus(sdetector_PN, {"withgid": True, "withtime": True, "to_file":False})

sdetector_LN_glo = nest.Create("spike_detector")
nest.SetStatus(sdetector_LN_glo, {"withgid": True, "withtime": True, "to_file":False})

# create a voltmeter for recording
mult = nest.Create('multimeter')
nest.SetStatus(mult, {"withtime":True, "record_from":["V_m"], "interval":5.})

# %%
################################# Connecting
# connection specification
conn_dict_1 = {'rule': 'pairwise_bernoulli', 'p': 0.1}
conn_dict_3 = {'rule': 'pairwise_bernoulli', 'p': 0.3}
conn_dict_4 = {'rule': 'pairwise_bernoulli', 'p': 0.4}
conn_dict_5 = {'rule': 'pairwise_bernoulli', 'p': 0.5}
conn_dict_out = {"rule": "fixed_outdegree", "outdegree":int(nLN_lat*0.7)}
conn_dict_all = {"rule": "all_to_all"}

# synapse specification
syn_exc = {'weight':{"distribution": "normal_clipped", "low":200., "mu":exc_weight, "sigma":100.}}
syn_inh_static = {'weight':{"distribution": "normal_clipped", "high":-50., "mu":inh_weight+50., "sigma":30.}}
syn_inh_stpd = {"model" :"stdp_synapse", 
           "tau_plus":5.,
           "Wmax":-1000.,
           'mu_minus': 1.0,
           'mu_plus': 1.0,
           'weight': {"distribution": "normal_clipped", "high":-50., "mu":inh_weight, "sigma":30.0}}

syn_inh_vogel = {"model" :"vogels_sprekeler_synapse", 
            'alpha': 0.5,
            'delay': 5.0,
            'eta': 0.5,
            'Kplus': 0.,
            'tau': 1.,
            "Wmax":-200.,
            'weight': {"distribution": "normal_clipped", "high":-50., "mu":inh_weight, "sigma":30.0}}

syn_inh_stp = {"model" : 'quantal_stp_synapse',
            'delay': 5.0,
            'tau_fac': 1.,
            'weight': {"distribution": "normal_clipped", "high":-50., "mu":inh_weight, "sigma":30.0}}

syn_inh = syn_inh_vogel


# connect ORNs
for i in range(glomeruli):
    nest.Connect((ORN[nORN*i:nORN*i+nORN]), (PN[nPN*i:nPN*i+nPN]), conn_dict_all, syn_exc)


for i in range(glomeruli):
    nest.Connect((ORN[nORN*i:nORN*i+nORN]), (LN_lat[nLN_lat*i:nLN_lat*i+nLN_lat]), conn_dict_all, syn_exc)

nest.Connect(ORN, LN_glo, conn_dict_3, syn_exc)
print("ORNs connected")

# connect LNs_glo
nest.Connect(LN_glo, PN, conn_dict_3, syn_inh_static)
nest.Connect(LN_glo, LN_lat, conn_dict_4, syn_inh_static)
nest.Connect(LN_glo, LN_glo, conn_dict_1, syn_inh_static)
print("global LNs connected")

# connect LNs_lat
for i in range(glomeruli):
    nest.Connect((LN_lat[nLN_lat*i:nLN_lat*i+nLN_lat]), (LN_lat[:nLN_lat*i]+LN_lat[nLN_lat*i+nLN_lat+1:]), conn_dict_4, syn_inh)
for i in range(glomeruli):
    nest.Connect((LN_lat[nLN_lat*i:nLN_lat*i+nLN_lat]), (PN[:nPN*i]+PN[nPN*i+nPN+1:]), conn_dict_5, syn_inh)
nest.Connect(LN_lat, LN_glo, conn_dict_3, syn_inh)
print("laterals LNs connected")


# connect N_rec excitatory / inhibitory neurons to spike detector
nest.Connect(ORN, sdetector_ORN)
nest.Connect(LN_lat, sdetector_LN_lat)
nest.Connect(PN, sdetector_PN)
nest.Connect(LN_glo, sdetector_LN_glo)

#connect multimeter 
nest.Connect(mult, [ORN[0]])
#nest.Connect(mult, LN_lat)
#nest.Connect(mult, LN_glo)
#nest.Connect(mult, PN)

# connect de Poisson generator to ORNs
#nest.Connect(pgen_1,ORN)
# connect the DC generator to ORNs
#nest.Connect(dcgen_1,ORN)
# connect the noise generator to ORNs
j=0
for i in st1_glom:
    nest.Connect(noise_gen_1,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st1_weights[j]})
    j+=1
    
j=0
for i in st2_glom:
    nest.Connect(noise_gen_2,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st2_weights[j]})
    j+=1

j=0
for i in st3_glom:
    nest.Connect(noise_gen_3,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st3_weights[j]})
    j+=1

j=0
for i in st4_glom:
    nest.Connect(noise_gen_4,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st4_weights[j]})
    j+=1
    
j=0
for i in st5_glom:
    nest.Connect(noise_gen_5,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st5_weights[j]})
    j+=1
    
j=0
for i in st6_glom:
    nest.Connect(noise_gen_6,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st6_weights[j]})
    j+=1
    
j=0
for i in st7_glom:
    nest.Connect(noise_gen_7,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st7_weights[j]})
    j+=1
    
j=0
for i in st8_glom:
    nest.Connect(noise_gen_8,glomeruli_list[i],conn_dict_all,syn_spec={"weight":st8_weights[j]})
    j+=1
    
# add noise to all neurons
nest.Connect(noise_gen,ORN)
nest.Connect(noise_gen,LN_lat)
nest.Connect(noise_gen,LN_glo)
nest.Connect(noise_gen,PN)


print("Rest of connections done")
# %%
# Connection matrix before experiments
conn= nest.GetConnections(ORN)
A = nest.GetStatus(conn,['source','target','weight'])
B = np.array(A)

conn1= nest.GetConnections(LN_lat)
A1 = nest.GetStatus(conn1,['source','target','weight'])
B1 = np.array(A1)


conn2= nest.GetConnections(LN_glo)
A2 = nest.GetStatus(conn2,['source','target','weight'])
B2 = np.array(A2)


C_source = np.array(B[:,0])
C2_source = np.array(B1[:,0])
C3_source = np.array(B2[:,0])

C_matrix = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))
C_matrix2 = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))
C_matrix3 = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))

for i in range(len(C_source)):
    C_matrix[int(B[i][0])][int(B[i][1])]=B[i][2]
    
    
for i in range(len(C2_source)):
    C_matrix[int(B1[i][0])][int(B1[i][1])]=B1[i][2]
    
for i in range(len(C3_source)):
    C_matrix[int(B2[i][0])][int(B2[i][1])]=B2[i][2]

     
#plt.figure()
fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(20,8),gridspec_kw={'width_ratios': [1]})
f1=axes.matshow(C_matrix[:,:], cmap='seismic', vmin=-max(abs(exc_weight),abs(inh_weight))-(max(abs(exc_weight),abs(inh_weight)))/10, vmax=max(abs(exc_weight),abs(inh_weight))+(max(abs(exc_weight),abs(inh_weight)))/10)
plt.colorbar(f1, ax=axes,fraction=0.025, pad=0.04)
plt.xlim(xmin=ORN[-1]-10)
plt.xlabel("TO (cell index)")
plt.ylabel("FROM (cell index)")
plt.title("Connection Matrix")
plt.show()

K_matrix = C_matrix

# %%
###################################################### Simulation

# run simulation for simtime ms
nest.Simulate(simtime)

print("simulation complete")

# %%
# read out recording time and voltage from voltmeter
spikes_ORN = nest.GetStatus(sdetector_ORN)[0]['events']['times']
index_ORN = nest.GetStatus(sdetector_ORN)[0]['events']['senders']

spikes_LN_lat = nest.GetStatus(sdetector_LN_lat)[0]['events']['times']
index_LN_lat = nest.GetStatus(sdetector_LN_lat)[0]['events']['senders']

spikes_PN = nest.GetStatus(sdetector_PN)[0]['events']['times']
index_PN = nest.GetStatus(sdetector_PN)[0]['events']['senders']

spikes_LN_glo = nest.GetStatus(sdetector_LN_glo)[0]['events']['times']
index_LN_glo = nest.GetStatus(sdetector_LN_glo)[0]['events']['senders']

times = nest.GetStatus(mult)[0]['events']['times']
voltages = nest.GetStatus(mult)[0]['events']['V_m']

## %%
##ORN
##for i in range (len(ORN)):
#
conn= nest.GetConnections(ORN)
A = nest.GetStatus(conn,['source','target','weight'])
B = np.array(A)

conn1= nest.GetConnections(LN_lat)
A1 = nest.GetStatus(conn1,['source','target','weight'])
B1 = np.array(A1)


conn2= nest.GetConnections(LN_glo)
A2 = nest.GetStatus(conn2,['source','target','weight'])
B2 = np.array(A2)


C_source = np.array(B[:,0])
C2_source = np.array(B1[:,0])
C3_source = np.array(B2[:,0])

C_matrix = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))
C_matrix2 = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))
C_matrix3 = np.zeros(shape = ((nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20, (nORN+nLN_lat+nLN_glo+nPN)*glomeruli+20))

for i in range(len(C_source)):
    C_matrix[int(B[i][0])][int(B[i][1])]=B[i][2]
    
    
for i in range(len(C2_source)):
    C_matrix[int(B1[i][0])][int(B1[i][1])]=B1[i][2]
    
for i in range(len(C3_source)):
    C_matrix[int(B2[i][0])][int(B2[i][1])]=B2[i][2]
    
    
    
submatrix= np.subtract(C_matrix, K_matrix)

# %%     
#plt.figure()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(20,8),gridspec_kw={'width_ratios': [1]})
f1=axes.matshow(C_matrix[:,:], cmap='seismic', vmin=-max(abs(exc_weight),abs(inh_weight))-(max(abs(exc_weight),abs(inh_weight)))/5, vmax=max(abs(exc_weight),abs(inh_weight))+(max(abs(exc_weight),abs(inh_weight)))/5)
plt.colorbar(f1, ax=axes,fraction=0.025, pad=0.04)
plt.xlim(xmin=ORN[-1]-10)
plt.xlabel("TO (cell index)")
plt.ylabel("FROM (cell index)")
plt.title("Connection Matrix")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(20,8),gridspec_kw={'width_ratios': [1]})
f1=axes.matshow(submatrix[:,:], cmap='seismic', vmin=-50, vmax=50)
plt.colorbar(f1, ax=axes,fraction=0.025, pad=0.04)
plt.xlim(xmin=ORN[-1]-10)
plt.xlabel("TO (cell index)")
plt.ylabel("FROM (cell index)")
plt.title("Connection Matrix")
plt.show()




    # %%
# Plotting
plt.figure(figsize = (10,5))
# raster plot of excitatory activity
plt.scatter(spikes_ORN, index_ORN, c = 'b',marker = "|",s = 20, label = "ORN")
plt.xlabel('Time (ms)')
plt.ylabel('# neuron')
plt.title('Raster plot')

# raster plot of inhibitory activity
plt.scatter(spikes_LN_lat, index_LN_lat, c = 'g',marker = "|",s = 20, label = "LN lateral")
plt.xlabel('Time (ms)')
plt.ylabel('# neuron')
plt.title('Raster plot')

plt.scatter(spikes_LN_glo, index_LN_glo, c='r',marker = "|",s = 20, label = "LN global")
plt.xlabel('Time (ms)')
plt.ylabel('# neuron')
plt.title('Raster plot')


plt.scatter(spikes_PN, index_PN, c='k', marker = "|", s = 20, label = "PN")
plt.xlabel('Time (ms)')
plt.ylabel('# neuron')
plt.title('Raster plot')
plt.legend(bbox_to_anchor=(1, 1), fontsize = 7)


plt.show()

nest.raster_plot.from_device(sdetector_PN, hist=True)
plt.show()

nest.raster_plot.from_device(sdetector_ORN, hist=True)
plt.show()

plt.figure()
plt.plot(times, voltages, label='ORN 1')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.title('Membrane potential')
plt.legend()
plt.show()

#%%
spikes_each_PN = []

for i in range(len(PN)):
    spikes_each_PN.append([])
    for k in range(len(index_PN)):
        if index_PN[k] == i+PN[0]:
            spikes_each_PN[i].append(spikes_PN[k])
           
           
all = []
all.clear()
for i in range(len(PN)):
    all.append([])
   
BIN = 120
bins_size = simtime/BIN

keys = []  
for i in range(len(PN)):
    P = np.histogram(spikes_each_PN[i], bins = BIN, range=[0,19000])
    all[i].extend(P[0])
    #creating keys for the dict
    keys.append(i)
   
pd.set_option('display.max_rows', 300)    
data = dict(zip(keys,all))

ALL = pd.DataFrame(data)

features = [i for i in range(len(PN))]# Separating out the features
x = ALL.loc[:,features].values
#x = StandardScaler().fit_transform(x)

pca = PCA(n_components=2)
principalComponents = pca.fit_transform(x)
principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1',\
                                                                  'principal component 2'])#,\
                                                                  #'principal component 3'])
                           
                           

x3 = np.array(principalDf.loc[:,['principal component 1']])
y3 = np.array(principalDf.loc[:,['principal component 2']])
#z3 = np.array(principalDf.loc[:,['principal component 3']])
                           
data = {}
x3 = np.ndarray.flatten(x3)
y3 = np.ndarray.flatten(y3)
#z3 = np.ndarray.flatten(z3)  



fig = plt.figure(figsize= (12,6))
#ax = plt.axes(projection='3d')
#
#ax.plot3D(x3,y3,z3)
#ax.scatter(x3[0], y3[0], z3[0], c = 'r', s = 30, label = "resting state")
#ax.plot3D(x3[0:lim],y3[0:lim],z3[0:lim], c = 'g', label = "stim1")
#ax.plot3D(x3[lim:2*lim],y3[lim:2*lim],z3[lim:2*lim], c = 'k', label = "stim2")
#ax.plot3D(x3[2*lim:3*lim],y3[2*lim:3*lim],z3[2*lim:3*lim] ,c = 'purple', label = "stim3")
#ax.plot3D(x3[3*lim:4*lim],y3[3*lim:4*lim], z3[3*lim:4*lim],c = 'y', label = "stim4")
##ax.plot3D(x3[4*lim:5*lim],y3[4*lim:5*lim], z3[4*lim:5*lim],c = 'yellowgreen', label = "stim5")
##ax.plot3D(x3[5*lim:6*lim],y3[5*lim:6*lim], z3[5*lim:6*lim],c = 'pink', label = "stim5")
#
#plt.show()
           


#plt.plot(x3,y3,label = "all dynamics")
plt.scatter(x3[0],y3[0], c = 'r', label = "resting state")
plt.plot(x3[0:int(st1_stop/bins_size + 1000/bins_size)],y3[0:int(st1_stop/bins_size + 1000/bins_size)], c = 'g', label = "stim1")
plt.plot(x3[int(st1_stop/bins_size + 1000/bins_size):int(st2_stop/bins_size + 1000/bins_size)],y3[int(st1_stop/bins_size + 1000/bins_size):int(st2_stop/bins_size + 1000/bins_size)], c = 'k', label = "stim2")
plt.plot(x3[int(st2_stop/bins_size + 1000/bins_size):int(st3_stop/bins_size + 1000/bins_size)],y3[int(st2_stop/bins_size + 1000/bins_size):int(st3_stop/bins_size + 1000/bins_size)], c = 'purple', label = "stim 1+2 PRE")
plt.plot(x3[int(st4_stop/bins_size - 1000/bins_size):int(st4_stop/bins_size + 1000/bins_size)],y3[int(st4_stop/bins_size - 1000/bins_size):int(st4_stop/bins_size + 1000/bins_size)], c = 'r', label = "stim 1+2 POST")
plt.legend()
plt.show()

#%%
# Intento de PCA 2
##PCA
#spikes_each_PN = []
#
#for i in range(len(PN)):
#    spikes_each_PN.append([])
#    for k in range(len(index_PN)):
#        if index_PN[k] == i+PN[0]:
#            spikes_each_PN[i].append(spikes_PN[k])
#           
#           
#all = []
#all.clear()
#for i in range(len(PN)):
#    all.append([])
#   
#BIN = int(dt)
#bins_size = int(simtime/BIN*10)
#
#keys = []  
#for i in range(len(PN)):
#    P = np.histogram(spikes_each_PN[i], bins = bins_size, range=[0,simtime])
#    all[i].extend(P[0])
#    #creating keys for the dict
#    keys.append(i)
#   
#pd.set_option('display.max_rows', bins_size)    
#data = dict(zip(keys,all))
#
#ALL = pd.DataFrame(data)
#
#features = [i for i in range(len(PN))]# Separating out the features
#x = ALL.loc[:,features].values
##x = StandardScaler().fit_transform(x)
#
#pca = PCA(n_components=2)
#principalComponents = pca.fit_transform(x)
#principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1',\
#                                                                  'principal component 2'])#,\
#                                                                  #'principal component 3'])
#                           
#                           
#
#x3 = np.array(principalDf.loc[:,['principal component 1']])
#y3 = np.array(principalDf.loc[:,['principal component 2']])
##z3 = np.array(principalDf.loc[:,['principal component 3']])
#                           
#data = {}
#x3 = np.ndarray.flatten(x3)
#y3 = np.ndarray.flatten(y3)
##z3 = np.ndarray.flatten(z3)  
#
#
#
#fig = plt.figure(figsize= (12,6))
##ax = plt.axes(projection='3d')
##
##ax.plot3D(x3,y3,z3)
##ax.scatter(x3[0], y3[0], z3[0], c = 'r', s = 30, label = "resting state")
##ax.plot3D(x3[0:lim],y3[0:lim],z3[0:lim], c = 'g', label = "stim1")
##ax.plot3D(x3[lim:2*lim],y3[lim:2*lim],z3[lim:2*lim], c = 'k', label = "stim2")
##ax.plot3D(x3[2*lim:3*lim],y3[2*lim:3*lim],z3[2*lim:3*lim] ,c = 'purple', label = "stim3")
##ax.plot3D(x3[3*lim:4*lim],y3[3*lim:4*lim], z3[3*lim:4*lim],c = 'y', label = "stim4")
###ax.plot3D(x3[4*lim:5*lim],y3[4*lim:5*lim], z3[4*lim:5*lim],c = 'yellowgreen', label = "stim5")
###ax.plot3D(x3[5*lim:6*lim],y3[5*lim:6*lim], z3[5*lim:6*lim],c = 'pink', label = "stim5")
##
##plt.show()
#           
#
#
##plt.plot(x3,y3,label = "all dynamics")
#plt.scatter(x3[0],y3[0], c = 'r', label = "resting state")
#plt.plot(x3[int(st1_start-20/BIN):int(st1_stop-20/BIN)],y3[int(st1_start-20/BIN):int(st1_stop-20/BIN)], c = 'g', label = "stim 1")
#plt.plot(x3[int(st2_start-20/BIN):int(st2_stop-20/BIN)],y3[int(st2_start-20/BIN):int(st2_stop-20/BIN)], c = 'k', label = "stim 2")
#plt.plot(x3[int(st3_start-20/BIN):int(st3_stop-20/BIN)],y3[int(st3_start-20/BIN):int(st3_stop-20/BIN)], c = 'purple', label = "stim 1+2")
#plt.plot(x3[int(st4_start-20/BIN):int(st4_stop-20/BIN)],y3[int(st4_start-20/BIN):int(st4_stop-20/BIN)], c = 'r', label = "stim 1+2")
#plt.legend()
#plt.show()